from django.shortcuts import render

# Create your views here.
name = "V"
bio_dict = [
    {'subject': 'Birth Date', 'value':'1 Januari'},
    {'subject': 'Genderender', 'value': 'Male'},
    {'subject': 'Description', 'value': 'I am anonymous'},
    {'subject': 'E-mail', 'value':'V@gmail.com'},
]
list_expertise = ['eat', 'drink', 'have fun']
author = "Tugas 1 PPW"

def index(request):
    response = {'bio' : bio_dict, "name" : name}
    response['expertise'] = list_expertise
    response['author'] = author
    return render(request, 'profilePage.html', response)