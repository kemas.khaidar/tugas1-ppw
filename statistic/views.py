from django.shortcuts import render
from Profile.views import name

# Create your views here.

def index(request):
    response = {"name" : name}
    return render(request, 'statistic/statistik.html', response)