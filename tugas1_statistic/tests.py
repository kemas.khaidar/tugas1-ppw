from django.test import TestCase
from django.test import Client
from .views import index
from django.urls import resolve
from tugas1_update_status.models import Status
from tugas1_Profile.views import name


class MenuStatsUnitTest(TestCase):

    # cek apakah url ada
	def test_menu_stats_is_exist(self):
		response = Client().get('/statistic/')
		self.assertEqual(response.status_code, 200)

	def test_menu_stats_using_index_func(self):
		found = resolve('/statistic/')
		self.assertEqual(found.func, index)
