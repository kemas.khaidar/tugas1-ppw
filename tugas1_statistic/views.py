from django.shortcuts import render
from django.http import HttpResponseRedirect
from tugas1_add_friend.models import Todo
from tugas1_update_status.models import Status
from tugas1_Profile.views import name

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Wilda" #TODO Implement yourname
    
    bnyk_temen = Todo.objects.all().count()
    response['friend']= bnyk_temen 

    bnyk_status = Status.objects.all().count()
    response['status'] = bnyk_status

    nama_profile = name
    response['nameP'] = nama_profile

    if bnyk_status > 0:
        response['lastT'] = Status.objects.last().created_date
        response['lastP'] = Status.objects.last().description
    else:
        response['lastT'] = "__-__"
        response['lastP'] = "no post"

    html = 'statistic/statistik.html'
    
    return render(request, html, response)
