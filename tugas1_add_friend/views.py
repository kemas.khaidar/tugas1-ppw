from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django import forms
from .forms import Friend_Form
from .models import Todo

# Create your views here.
response = {} #TODO Implement yourname
html = 'add_friend.html'
def index(request):
    friend = Todo.objects.all()
    response['author'] = 'Tugas 1 PPW'
    response['friend'] = friend
    response['friend_form'] = Friend_Form
    return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        if(url_is_valid(request.POST['url'])):
            friend = Todo(name=request.POST['name'], url=request.POST['url'])
            friend.save()   
            return redirect('/friend/',{'error':False})
    return redirect('/friend/',{'error':True})

def url_is_valid(url):
    url_form_field = forms.URLField()
    try:
        url = url_form_field.clean(url)
    except ValidationError:
        return False
    return True
