from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Todo
from .forms import Friend_Form

# Create your tests here.

class AddFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/friend/')
		self.assertEqual(response.status_code, 200)
		
	def test_add_friend_using_index_func(self):
		found = resolve('/friend/')
		self.assertEqual(found.func, index)
		
	def test_model_can_create_new_friend(self):
		#Creating a new friend object
		new_friend = Todo.objects.create(name='John Titor',url='http://heroku-app-name.herokuapp.com')
		
		#Retrieving all available friend object
		counting_all_available_friend = Todo.objects.all().count()
		self.assertEqual(counting_all_available_friend, 1)
	
	def test_form_has_placeholder_and_css_classes(self):
		form = Friend_Form()
		self.assertIn('class="form-control"', form.as_p())
		self.assertIn('<label for="id_name">Nama:</label>', form.as_p())
		self.assertIn('<label for="id_url">URL:</label>', form.as_p())
		
	def test_form_validation_for_blank_items(self):
		form = Friend_Form(data={'name': '', 'url': ''})
		self.assertFalse(form.is_valid())
		# self.assertEqual(form.errors['messages'], ["Isi input dengan url"])
		
	def test_add_friend_post_fail(self):
		response = Client().post('/add_friend/friend', {'name': 'John Titor', 'url': ''})
		self.assertEqual(response.status_code, 302)
		
	def test_add_friend_post_success_and_render_the_result(self):
		name = 'John Titor'
		url = 'http://herokuapp.com'
		response = Client().post('/add_friend/friend', {'name': name, 'url': url})
		self.assertEqual(response.status_code, 302)
		response = Client().get('/friend/')
		html_response = response.content.decode('utf8')
		self.assertIn(name, html_response)
		self.assertIn(url, html_response)
